# Katas

## Roman Numeral

- I -> 1
- V -> 5
- X -> 10
- OPTIONAL: Z -> InvalidDigitException
- II -> 2
- XX -> 20
- VII -> 7
- IV -> 4
- MMXVIII -> 2018
- MCMXCIX -> 1999

## PrimeFactors (Reactor)
- 1 -> []
- 2 -> [2]
- 3 -> [3]
- 4 -> [2, 2]
- 6 -> [2, 3]
- 8 -> [2, 2, 2]
- 9 -> [3, 3]
- 2 * 2 * 3 * 5 * 7 * 11 * 13 -> [2, 2, 3, 5, 7, 11, 13]


## Stack

- new_stack_is_empty

- after_one_push_stack_size_should_be_one

- after_one_push_and_one_pop_stack_size_should_be_zero

- when_pushed_past_limit_stack_overflows

- when_empty_stack_popped_stack_underflows

- when_one_is_pushed_one_is_popped

- when_one_and_two_are_pushed_two_and_one_are_popped

- when_creating_stack_with_negativ_size_stack_should_throw_illegal_capacity

- when_stack_created_with_zero_capacity_any_push_should_overlow

- when_one_is_pushed_one_is_on_top

- when_stack_is_empty_top_throws_empty

- when_zero_capacity_stack_top_throws_empty

- given_stack_with_one_and_two_pushed_find_one

- given_stack_with_no_two_find_two_should_return_null (Optional.empty())


## Bowling 

- Gutter Game
- All One
- Spare
- Strike
- Perfect Game

## Tennis Game (Reactor - Pub - Sub)

- Love all (new game)
- Fifteen, Love (1, 0)
- Forty, Thirty (3, 2)
- Player Two wins (0, 4)
- Deuce (3, 3)
- Advantage Player One (name argument optional) (4, 3)
- Player One wins (name argument optional) (5, 3)
- Deuce (4, 4)
- Advantage Player Two (4, 5)
- Player Two wins (4, 6)

## Video Store

- Refactoring