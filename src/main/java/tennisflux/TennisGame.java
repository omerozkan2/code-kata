package tennisflux;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import reactor.core.publisher.Flux;

/**
 * @author Omer Ozkan
 */
public class TennisGame {
    private static Map<Integer, String> SCORE_NAMES = new HashMap<Integer, String>() {{
        put(0, "Love");
        put(1, "Fifteen");
        put(2, "Thirty");
        put(3, "Forty");
    }};
    private AtomicBoolean gameOver = new AtomicBoolean(false);

    private List<ScoreListener> listeners = new ArrayList<>();
    private List<String> existingScores = Collections.synchronizedList(new ArrayList<>(Collections.singleton("Love all")));

    private int playerOneScore;
    private int playerTwoScore;
    private Flux<String> flux;

    public Flux<String> score() {
        if (flux == null) {
            flux = Flux.create(sink -> {
                listeners.add(new ScoreListener() {
                    @Override
                    public void publishNewScore(String score) {
                        sink.next(score);
                    }

                    @Override
                    public void complete() {
                        sink.complete();
                    }
                });

                sink.onRequest(n -> {
                    existingScores.forEach(sink::next);
                    if (gameOver.get()) {
                        sink.complete();
                    }
                });
            });
        }

        return flux;
    }

    public void playerOneScores() {
        playerOneScore++;
        publishNewScore();
    }

    private void publishNewScore() {
        String newScore = getCurrentScoreName();

        existingScores.add(newScore);
        listeners.forEach(l -> l.publishNewScore(newScore));

        if (gameOver.get()) {
            listeners.forEach(ScoreListener::complete);
        }
    }

    private String getCurrentScoreName() {

        if (playerOneScore > playerTwoScore + 1 && playerOneScore > 3) {
            gameOver.set(true);
            return "Player One wins";
        }

        if (playerOneScore == playerTwoScore + 1 && playerOneScore > 3) {
            return "Advantage Player One";
        }

        if (playerTwoScore == playerOneScore + 1 && playerTwoScore > 3) {
            return "Advantage Player Two";
        }

        if (playerOneScore == playerTwoScore && playerOneScore == 3) {
            return "Deuce";
        }

        if (playerOneScore == playerTwoScore) {
            return String.format("%s all", SCORE_NAMES.get(playerOneScore));
        } else {
            return String.format("%s, %s", SCORE_NAMES.get(playerOneScore), SCORE_NAMES.get(playerTwoScore));
        }
    }

    public void playerTwoScores() {
        playerTwoScore++;
        publishNewScore();
    }

    private interface ScoreListener {
        void publishNewScore(String score);

        void complete();
    }
}
