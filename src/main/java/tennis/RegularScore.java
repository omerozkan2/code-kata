package tennis;

/**
 * @author Omer Ozkan
 */
public class RegularScore implements Score {
    private PlayerScore playerOneScore;
    private PlayerScore playerTwoScore;

    public RegularScore() {
        this.playerOneScore = PlayerScore.LOVE;
        this.playerTwoScore = PlayerScore.LOVE;
    }

    private RegularScore(PlayerScore playerOneScore, PlayerScore playerTwoScore) {
        this.playerOneScore = playerOneScore;
        this.playerTwoScore = playerTwoScore;
    }

    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        if (playerOneScore != playerTwoScore) {
            return String.format("%s, %s", playerOneScore.getName(), playerTwoScore.getName());
        }
        return String.format("%s all", playerOneScore.getName());
    }

    @Override
    public Score playerOneScores() {
        final PlayerScore p1NextScore = playerOneScore.increment();
        if (isDeuce(p1NextScore, playerTwoScore)) {
            return new Deuce();
        }

        if (playerOneScore == PlayerScore.FORTY) {
            return new PlayerOneWins();
        }
        return new RegularScore(p1NextScore, playerTwoScore);
    }

    private boolean isDeuce(PlayerScore p1Score, PlayerScore p2Score) {
        return p1Score == PlayerScore.FORTY && p2Score == PlayerScore.FORTY;
    }

    @Override
    public Score playerTwoScores() {
        final PlayerScore p2NextScore = playerTwoScore.increment();

        if (isDeuce(playerOneScore, p2NextScore)) {
            return new Deuce();
        }

        if (playerTwoScore == PlayerScore.FORTY) {
            return new PlayerTwoWins();
        }

        return new RegularScore(playerOneScore, p2NextScore);
    }

    private enum PlayerScore {
        LOVE {
            @Override
            PlayerScore increment() {
                return FIFTEEN;
            }
        },
        FIFTEEN {
            @Override
            PlayerScore increment() {
                return THIRTY;
            }
        },
        THIRTY {
            @Override
            PlayerScore increment() {
                return FORTY;
            }
        },
        FORTY {
            @Override
            PlayerScore increment() {
                return FORTY;
            }
        };


        abstract PlayerScore increment();

        public String getName() {
            return this.name().substring(0, 1).toUpperCase() + this.name().substring(1).toLowerCase();
        }
    }
}
