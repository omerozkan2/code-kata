package tennis;

/**
 * @author Omer Ozkan
 */
public class TennisGameImplV2 implements TennisGame {

    private Score score = new RegularScore();
    private final String playerOneName;
    private final String playerTwoName;

    public TennisGameImplV2(String playerOneName, String playerTwoName) {
        this.playerOneName = playerOneName;
        this.playerTwoName = playerTwoName;
    }

    @Override
    public String score() {
        return score.getAsString(playerOneName, playerTwoName);
    }

    @Override
    public void playerOneScores() {
        this.score = score.playerOneScores();
    }

    @Override
    public void playerTwoScores() {
        this.score = score.playerTwoScores();
    }
}
