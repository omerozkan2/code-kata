package tennis;

/**
 * @author Omer Ozkan
 */
public interface Score {
    String getAsString(String playerOneName, String playerTwoName);

    Score playerOneScores();

    Score playerTwoScores();
}
