package tennis;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Omer Ozkan
 */
public class TennisGameImpl implements TennisGame {

    private static Map<Integer, String> SCORE_NAMES = new HashMap<Integer, String>() {{
        put(0, "Love");
        put(1, "Fifteen");
        put(2, "Thirty");
        put(3, "Forty");
    }};

    private int playerOneScore = 0;
    private int playerTwoScore = 0;
    private String playerOneName;
    private String playerTwoName;

    public TennisGameImpl(String playerOneName, String playerTwoName) {
        this.playerOneName = playerOneName;
        this.playerTwoName = playerTwoName;
    }

    @Override
    public String score() {

        if (isPlayerOneWinner()) {
            return playerOneName + " wins";
        }

        if (isPlayerTwoWinner()) {
            return playerTwoName + " wins";
        }

        if (playerTwoHasAdvantage()) {
            return "Advantage " + playerTwoName;
        }

        if (playerOneHasAdvantage()) {
            return "Advantage " + playerOneName;
        }

        if (playerTwoScore != playerOneScore) {
            return generateScoreNames();
        }

        return playerOneScore > 2 ? "Deuce" : String.format("%s all", getPlayerOneScoreName());
    }

    private boolean playerOneHasAdvantage() {
        return playerOneScore == playerTwoScore + 1 && playerOneScore > 3;
    }

    private boolean playerTwoHasAdvantage() {
        return playerTwoScore == playerOneScore + 1 && playerTwoScore > 3;
    }

    private boolean isPlayerTwoWinner() {
        return playerTwoScore > playerOneScore + 1 && playerTwoScore > 3;
    }

    private boolean isPlayerOneWinner() {
        return playerOneScore > playerTwoScore + 1 && playerOneScore > 3;
    }

    private String generateScoreNames() {
        return String.format("%s, %s", getPlayerOneScoreName(), getPlayerTwoScoreName());
    }

    private String getPlayerTwoScoreName() {
        return SCORE_NAMES.get(playerTwoScore);
    }

    private String getPlayerOneScoreName() {
        return SCORE_NAMES.get(playerOneScore);
    }

    @Override
    public void playerOneScores() {
        this.playerOneScore++;
    }

    @Override
    public void playerTwoScores() {
        this.playerTwoScore++;
    }

}
