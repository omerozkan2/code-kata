package tennis;

/**
 * @author Omer Ozkan
 */
public interface TennisGame {
    String score();

    void playerOneScores();

    void playerTwoScores();
}
