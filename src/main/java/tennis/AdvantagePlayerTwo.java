package tennis;

/**
 * @author Omer Ozkan
 */
public class AdvantagePlayerTwo implements Score {
    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        return "Advantage " + playerTwoName;
    }

    @Override
    public Score playerOneScores() {
        return new Deuce();
    }

    @Override
    public Score playerTwoScores() {
        return new PlayerTwoWins();
    }
}
