package tennis;

/**
 * @author Omer Ozkan
 */
public class PlayerOneWins implements Score {
    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        return playerOneName + " wins";
    }

    @Override
    public Score playerOneScores() {
        return this;
    }

    @Override
    public Score playerTwoScores() {
        return this;
    }
}
