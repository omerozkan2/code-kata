package tennis;

/**
 * @author Omer Ozkan
 */
public class Deuce implements Score {
    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        return "Deuce";
    }

    @Override
    public Score playerOneScores() {
        return new AdvantagePlayerOne();
    }

    @Override
    public Score playerTwoScores() {
        return new AdvantagePlayerTwo();
    }
}
