package tennis;

/**
 * @author Omer Ozkan
 */
public class PlayerTwoWins implements Score {
    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        return playerTwoName + " wins";
    }

    @Override
    public Score playerOneScores() {
        return this;
    }

    @Override
    public Score playerTwoScores() {
        return this;
    }
}
