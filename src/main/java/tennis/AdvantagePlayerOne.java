package tennis;

/**
 * @author Omer Ozkan
 */
public class AdvantagePlayerOne implements Score {
    @Override
    public String getAsString(String playerOneName, String playerTwoName) {
        return "Advantage " + playerOneName;
    }

    @Override
    public Score playerOneScores() {
        return new PlayerOneWins();
    }

    @Override
    public Score playerTwoScores() {
        return new Deuce();
    }
}
