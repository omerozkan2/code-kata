package stackkata;

/**
 * @author Omer Ozkan
 */
public class Stack {
    private int size;
    private int[] elements;


    public Stack(int capacity) {
        this.elements = new int[capacity];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void push(int element) {
        if (size == elements.length) {
            throw new Overflow();
        }
        this.elements[size++] = element;
    }

    public int size() {
        return size;
    }

    public int pop() {
        if (size == 0) {
            throw new Underflow();
        }
        return elements[--size];
    }

    public int top() {
        return elements[size - 1];
    }

    public static class Overflow extends IllegalStateException {
    }

    public static class Underflow extends IllegalStateException {
    }
}
