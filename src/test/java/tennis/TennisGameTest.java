package tennis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Omer Ozkan
 */
public class TennisGameTest {

    private TennisGame game;
    private static final String PLAYER_ONE = "PlayerOne";
    private static final String PLAYER_TWO = "PlayerTwo";

    @BeforeEach
    void setUp() {
        game = new TennisGameImplV2(PLAYER_ONE, PLAYER_TWO);
    }

    @DisplayName("generate score as expected")
    @ParameterizedTest(name = "{0} -> {1}, {2}")
    @MethodSource("data")
    void generateScoreAsExpected(String expected, int playerOneScore, int playerTwoScore) {
        scoreMany(playerOneScore, playerTwoScore);
        assertEquals(expected, game.score());
    }

    @Test
    void deuceAfterAdvantagePlayerOne() {
        scoreMany(3, 3);
        game.playerOneScores();
        game.playerTwoScores();
        assertEquals("Deuce", game.score());
    }

    @Test
    void deuceAfterAdvantagePlayerTwo() {
        scoreMany(3, 3);
        game.playerTwoScores();
        game.playerOneScores();
        assertEquals("Deuce", game.score());
    }

    private void scoreMany(int playerOneScore, int playerTwoScore) {
        final int difference = playerOneScore - playerTwoScore;
        int commonScore = playerOneScore < playerTwoScore ? playerOneScore : playerTwoScore;

        for (int i = 0; i < commonScore; i++) {
            game.playerOneScores();
            game.playerTwoScores();
        }

        if (difference > 0) {
            for (int i = 0; i < playerOneScore - commonScore; i++) {
                game.playerOneScores();
            }
        } else {
            for (int i = 0; i < playerTwoScore - commonScore; i++) {
                game.playerTwoScores();
            }
        }
    }

    private static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of("Love all", 0, 0),
                Arguments.of("Fifteen, Love", 1, 0),
                Arguments.of("Thirty, Forty", 2, 3),
                Arguments.of("Forty, Love", 3, 0),
                Arguments.of("Thirty all", 2, 2),
                Arguments.of("Deuce", 3, 3),
                Arguments.of("Advantage " + PLAYER_ONE, 4, 3),
                Arguments.of("Advantage " + PLAYER_TWO, 3, 4),
                Arguments.of("Advantage " + PLAYER_ONE, 5, 4),
                Arguments.of("Advantage " + PLAYER_TWO, 4, 5),
                Arguments.of(PLAYER_ONE + " wins", 4, 1),
                Arguments.of(PLAYER_TWO + " wins", 1, 4),
                Arguments.of(PLAYER_ONE + " wins", 6, 4),
                Arguments.of(PLAYER_TWO + " wins", 4, 6)
        );
    }


}
