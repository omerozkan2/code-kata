package stackkata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Omer Ozkan
 */
public class StackTest {

    private final Stack stack = new Stack(2);

    @Test
    void newStackIsEmpty() {
        assertTrue(stack.isEmpty());
    }

    @Test
    void oneElementPushedThenStackSizeShouldBeOne() {
        stack.push(1);
        assertFalse(stack.isEmpty());
        assertEquals(1, stack.size());
    }

    @Test
    void oneElementPushedAndOneElementPoppedThenStackShouldBeEmpty() {
        stack.push(1);
        stack.pop();
        assertEquals(0, stack.size());
        assertTrue(stack.isEmpty());
    }

    @Test
    void whenOneIsPushedThenPopShouldBeOne() {
        stack.push(1);
        assertEquals(1, stack.pop());
    }

    @Test
    void whenOneAndTwoPushedThenPopShouldBeTwoAndOne() {
        stack.push(1);
        stack.push(2);
        assertEquals(2, stack.pop());
        assertEquals(1, stack.pop());
    }

    @Test
    void whenStackIsFullThenPushThrowsOverflow() {
        final Stack zeroCapacityStack = new Stack(0);
        assertThrows(Stack.Overflow.class, () -> zeroCapacityStack.push(1));
    }

    @Test
    void whenStackIsEmptyPopThrowsUnderflow() {
        final Stack emptyStack = new Stack(2);
        assertThrows(Stack.Underflow.class, emptyStack::pop);
    }

    @Test
    void whenOneAndTwoPushedThenTopShouldReturnTwo() {
        stack.push(1);
        stack.push(2);
        assertEquals(2, stack.top());
        assertEquals(2, stack.size());
    }
}
