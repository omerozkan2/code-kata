package videostore;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RentalTest {
    private Customer customer = new Customer("Customer");

    @Test
    public void testWhenRentedANewRelase_AmountShouldBe_9_0() throws Exception {
        customer.addRental(new Rental(new Movie("Rambo", Movie.NEW_RELEASE), 3));
        assertEquals("" + "Rental Record for Customer\n" + "\tRambo\t9.0\n" + "Amount owed is 9.0\n"
                + "You earned 2 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedANewRelase_AmountShouldBe_3_0() throws Exception {
        customer.addRental(new Rental(new Movie("Rambo", Movie.NEW_RELEASE), 1));
        assertEquals("" + "Rental Record for Customer\n" + "\tRambo\t3.0\n" + "Amount owed is 3.0\n"
                + "You earned 1 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedTwoNewRelase_AmountShouldBe_18_0() throws Exception {
        customer.addRental(new Rental(new Movie("Rambo", Movie.NEW_RELEASE), 3));
        customer.addRental(new Rental(new Movie("Rambo2", Movie.NEW_RELEASE), 3));
        assertEquals("" + "Rental Record for Customer\n" + "\tRambo\t9.0\n\tRambo2\t9.0\n" + "Amount owed is 18.0\n"
                + "You earned 4 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedAChildrenMovie_AmountShouldBe_1_5() throws Exception {
        customer.addRental(new Rental(new Movie("Winipoo", Movie.CHILDRENS), 2));
        assertEquals("" + "Rental Record for Customer\n" + "\tWinipoo\t1.5\n" + "Amount owed is 1.5\n"
                + "You earned 1 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedAChildrenMovieForFourDays_AmountShouldBe_1_5() throws Exception {
        customer.addRental(new Rental(new Movie("Winipoo", Movie.CHILDRENS), 4));
        assertEquals("" + "Rental Record for Customer\n" + "\tWinipoo\t3.0\n" + "Amount owed is 3.0\n"
                + "You earned 1 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedARegular_AmountShouldBe_2_0() throws Exception {
        customer.addRental(new Rental(new Movie("Winipoa", Movie.REGULAR), 1));
        assertEquals("" + "Rental Record for Customer\n" + "\tWinipoa\t2.0\n" + "Amount owed is 2.0\n"
                + "You earned 1 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedARegularMovieForSixDays_AmountShouldBe_8_0() throws Exception {
        customer.addRental(new Rental(new Movie("Winipoa", Movie.REGULAR), 6));
        assertEquals("" + "Rental Record for Customer\n" + "\tWinipoa\t8.0\n" + "Amount owed is 8.0\n"
                + "You earned 1 frequent renter points", customer.statement());
    }

    @Test
    public void testWhenRentedTwoRegularMoviesForSixDays_AmountShouldBe_16_0() throws Exception {
        customer.addRental(new Rental(new Movie("Winipoa", Movie.REGULAR), 6));
        customer.addRental(new Rental(new Movie("Winipoa2", Movie.REGULAR), 6));
        assertEquals("" + "Rental Record for Customer\n" + "\tWinipoa\t8.0\n" + "\tWinipoa2\t8.0\n"
                + "Amount owed is 16.0\n" + "You earned 2 frequent renter points", customer.statement());
    }
}
