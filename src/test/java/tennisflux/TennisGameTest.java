package tennisflux;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * Unfinished
 *
 * @author Omer Ozkan
 */
public class TennisGameTest {

    private final TennisGame game = new TennisGame();

    @Test
    void newGameLoveAll() {
        Flux<String> result = game.score();
        StepVerifier.create(result)
                .expectNext("Love all")
                .thenCancel()
                .verify();
    }

    @Test
    void playerOneScores() {
        Flux<String> result = game.score().log();
        game.playerOneScores();

        StepVerifier.create(result)
                .expectNext("Love all")
                .expectNext("Fifteen, Love")
                .thenCancel()
                .verify();
    }

    @Test
    void multipleListener() throws InterruptedException {
        final ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.execute(() -> {
            StepVerifier.create(game.score())
                    .expectNext("Love all")
                    .thenAwait(Duration.ofSeconds(1))
                    .expectNext("Fifteen, Love")
                    .thenCancel()
                    .verify();
        });
        Thread.sleep(500);
        game.playerOneScores();

        StepVerifier.create(game.score())
                .expectNext("Love all")
                .expectNext("Fifteen, Love")
                .thenCancel()
                .verify(Duration.ofSeconds(1));

    }

    @Test
    void fifteenAll() {
        game.playerOneScores();
        game.playerTwoScores();

        StepVerifier.create(game.score().log())
                .expectNext("Love all")
                .expectNext("Fifteen, Love")
                .expectNext("Fifteen all")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    void loveThirty() {
        game.playerTwoScores();
        game.playerTwoScores();

        StepVerifier.create(game.score().log())
                .expectNext("Love all")
                .expectNext("Love, Fifteen")
                .expectNext("Love, Thirty")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    void fortyLove() {
        game.playerOneScores();
        game.playerOneScores();
        game.playerOneScores();

        StepVerifier.create(game.score().log())
                .expectNext("Love all", "Fifteen, Love", "Thirty, Love")
                .expectNext("Forty, Love")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    void deuce() {
        playerOneScoresMany(3);
        playerTwoScoresMany(3);

        StepVerifier.create(game.score().log())
                .expectNext("Love all")
                .expectNext("Fifteen, Love", "Thirty, Love", "Forty, Love")
                .expectNext("Forty, Fifteen", "Forty, Thirty")
                .expectNext("Deuce")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    public void playerOneScoresMany(int times) {
        Flux.range(0, times)
                .subscribe((n) -> game.playerOneScores());
    }

    public void playerTwoScoresMany(int times) {
        Flux.range(0, times)
                .subscribe((n) -> game.playerTwoScores());
    }

    @Test
    void advantagePlayerTwo() {
        playerOneScoresMany(3);
        playerTwoScoresMany(3);

        game.playerTwoScores();

        StepVerifier.create(game.score().log())
                .expectNextCount(6)
                .expectNext("Deuce")
                .expectNext("Advantage Player Two")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    void advantagePlayerOne() {
        playerOneScoresMany(3);
        playerTwoScoresMany(3);

        game.playerOneScores();

        StepVerifier.create(game.score().log())
                .expectNextCount(6)
                .expectNext("Deuce")
                .expectNext("Advantage Player One")
                .thenCancel()
                .verify(Duration.ofSeconds(1));
    }

    @Test
    void playerOneWins() {
        playerTwoScoresMany(3);
        playerOneScoresMany(3);

        game.playerOneScores();
        game.playerOneScores();

        StepVerifier.create(game.score().log())
                .expectNextCount(6)
                .expectNext("Deuce")
                .expectNext("Advantage Player One")
                .expectNext("Player One wins")
                .verifyComplete();
    }
}
